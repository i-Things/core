// Code generated by goctl. DO NOT EDIT.
// Source: sys.proto

package modulemanage

import (
	"context"

	"gitee.com/i-Things/core/service/syssvr/internal/svc"
	"gitee.com/i-Things/core/service/syssvr/pb/sys"

	"github.com/zeromicro/go-zero/zrpc"
	"google.golang.org/grpc"
)

type (
	AccessInfo                            = sys.AccessInfo
	AccessInfoIndexReq                    = sys.AccessInfoIndexReq
	AccessInfoIndexResp                   = sys.AccessInfoIndexResp
	AccessInfoMultiImportReq              = sys.AccessInfoMultiImportReq
	AccessInfoMultiImportResp             = sys.AccessInfoMultiImportResp
	ApiInfo                               = sys.ApiInfo
	ApiInfoIndexReq                       = sys.ApiInfoIndexReq
	ApiInfoIndexResp                      = sys.ApiInfoIndexResp
	AppInfo                               = sys.AppInfo
	AppInfoIndexReq                       = sys.AppInfoIndexReq
	AppInfoIndexResp                      = sys.AppInfoIndexResp
	AppModuleIndexReq                     = sys.AppModuleIndexReq
	AppModuleIndexResp                    = sys.AppModuleIndexResp
	AppModuleMultiUpdateReq               = sys.AppModuleMultiUpdateReq
	AppPolicy                             = sys.AppPolicy
	AppPolicyReadReq                      = sys.AppPolicyReadReq
	AreaInfo                              = sys.AreaInfo
	AreaInfoIndexReq                      = sys.AreaInfoIndexReq
	AreaInfoIndexResp                     = sys.AreaInfoIndexResp
	AreaInfoReadReq                       = sys.AreaInfoReadReq
	AreaProfile                           = sys.AreaProfile
	AreaProfileIndexReq                   = sys.AreaProfileIndexReq
	AreaProfileIndexResp                  = sys.AreaProfileIndexResp
	AreaProfileReadReq                    = sys.AreaProfileReadReq
	AreaWithID                            = sys.AreaWithID
	AuthApiInfo                           = sys.AuthApiInfo
	ConfigResp                            = sys.ConfigResp
	DataArea                              = sys.DataArea
	DataAreaIndexReq                      = sys.DataAreaIndexReq
	DataAreaIndexResp                     = sys.DataAreaIndexResp
	DataAreaMultiDeleteReq                = sys.DataAreaMultiDeleteReq
	DataAreaMultiUpdateReq                = sys.DataAreaMultiUpdateReq
	DataProject                           = sys.DataProject
	DataProjectDeleteReq                  = sys.DataProjectDeleteReq
	DataProjectIndexReq                   = sys.DataProjectIndexReq
	DataProjectIndexResp                  = sys.DataProjectIndexResp
	DataProjectMultiUpdateReq             = sys.DataProjectMultiUpdateReq
	DataProjectSaveReq                    = sys.DataProjectSaveReq
	DateRange                             = sys.DateRange
	DictDetail                            = sys.DictDetail
	DictDetailIndexReq                    = sys.DictDetailIndexReq
	DictDetailIndexResp                   = sys.DictDetailIndexResp
	DictInfo                              = sys.DictInfo
	DictInfoIndexReq                      = sys.DictInfoIndexReq
	DictInfoIndexResp                     = sys.DictInfoIndexResp
	DictInfoReadReq                       = sys.DictInfoReadReq
	Empty                                 = sys.Empty
	IDList                                = sys.IDList
	JwtToken                              = sys.JwtToken
	LoginLogCreateReq                     = sys.LoginLogCreateReq
	LoginLogIndexReq                      = sys.LoginLogIndexReq
	LoginLogIndexResp                     = sys.LoginLogIndexResp
	LoginLogInfo                          = sys.LoginLogInfo
	Map                                   = sys.Map
	MenuInfo                              = sys.MenuInfo
	MenuInfoIndexReq                      = sys.MenuInfoIndexReq
	MenuInfoIndexResp                     = sys.MenuInfoIndexResp
	MessageInfo                           = sys.MessageInfo
	MessageInfoIndexReq                   = sys.MessageInfoIndexReq
	MessageInfoIndexResp                  = sys.MessageInfoIndexResp
	MessageInfoSendReq                    = sys.MessageInfoSendReq
	ModuleInfo                            = sys.ModuleInfo
	ModuleInfoIndexReq                    = sys.ModuleInfoIndexReq
	ModuleInfoIndexResp                   = sys.ModuleInfoIndexResp
	NotifyChannel                         = sys.NotifyChannel
	NotifyChannelIndexReq                 = sys.NotifyChannelIndexReq
	NotifyChannelIndexResp                = sys.NotifyChannelIndexResp
	NotifyConfig                          = sys.NotifyConfig
	NotifyConfigIndexReq                  = sys.NotifyConfigIndexReq
	NotifyConfigIndexResp                 = sys.NotifyConfigIndexResp
	NotifyConfigSendReq                   = sys.NotifyConfigSendReq
	NotifyConfigTemplate                  = sys.NotifyConfigTemplate
	NotifyConfigTemplateDeleteReq         = sys.NotifyConfigTemplateDeleteReq
	NotifyConfigTemplateIndexReq          = sys.NotifyConfigTemplateIndexReq
	NotifyConfigTemplateIndexResp         = sys.NotifyConfigTemplateIndexResp
	NotifyTemplate                        = sys.NotifyTemplate
	NotifyTemplateIndexReq                = sys.NotifyTemplateIndexReq
	NotifyTemplateIndexResp               = sys.NotifyTemplateIndexResp
	OperLogCreateReq                      = sys.OperLogCreateReq
	OperLogIndexReq                       = sys.OperLogIndexReq
	OperLogIndexResp                      = sys.OperLogIndexResp
	OperLogInfo                           = sys.OperLogInfo
	OpsFeedback                           = sys.OpsFeedback
	OpsFeedbackIndexReq                   = sys.OpsFeedbackIndexReq
	OpsFeedbackIndexResp                  = sys.OpsFeedbackIndexResp
	OpsWorkOrder                          = sys.OpsWorkOrder
	OpsWorkOrderIndexReq                  = sys.OpsWorkOrderIndexReq
	OpsWorkOrderIndexResp                 = sys.OpsWorkOrderIndexResp
	PageInfo                              = sys.PageInfo
	PageInfo_OrderBy                      = sys.PageInfo_OrderBy
	Point                                 = sys.Point
	ProjectAuth                           = sys.ProjectAuth
	ProjectInfo                           = sys.ProjectInfo
	ProjectInfoIndexReq                   = sys.ProjectInfoIndexReq
	ProjectInfoIndexResp                  = sys.ProjectInfoIndexResp
	ProjectProfile                        = sys.ProjectProfile
	ProjectProfileIndexReq                = sys.ProjectProfileIndexReq
	ProjectProfileIndexResp               = sys.ProjectProfileIndexResp
	ProjectProfileReadReq                 = sys.ProjectProfileReadReq
	ProjectWithID                         = sys.ProjectWithID
	QRCodeReadReq                         = sys.QRCodeReadReq
	QRCodeReadResp                        = sys.QRCodeReadResp
	RoleAccessIndexReq                    = sys.RoleAccessIndexReq
	RoleAccessIndexResp                   = sys.RoleAccessIndexResp
	RoleAccessMultiUpdateReq              = sys.RoleAccessMultiUpdateReq
	RoleApiAuthReq                        = sys.RoleApiAuthReq
	RoleAppIndexReq                       = sys.RoleAppIndexReq
	RoleAppIndexResp                      = sys.RoleAppIndexResp
	RoleAppMultiUpdateReq                 = sys.RoleAppMultiUpdateReq
	RoleAppUpdateReq                      = sys.RoleAppUpdateReq
	RoleInfo                              = sys.RoleInfo
	RoleInfoIndexReq                      = sys.RoleInfoIndexReq
	RoleInfoIndexResp                     = sys.RoleInfoIndexResp
	RoleMenuIndexReq                      = sys.RoleMenuIndexReq
	RoleMenuIndexResp                     = sys.RoleMenuIndexResp
	RoleMenuMultiUpdateReq                = sys.RoleMenuMultiUpdateReq
	RoleModuleIndexReq                    = sys.RoleModuleIndexReq
	RoleModuleIndexResp                   = sys.RoleModuleIndexResp
	RoleModuleMultiUpdateReq              = sys.RoleModuleMultiUpdateReq
	SlotInfo                              = sys.SlotInfo
	SlotInfoIndexReq                      = sys.SlotInfoIndexReq
	SlotInfoIndexResp                     = sys.SlotInfoIndexResp
	TenantAccessIndexReq                  = sys.TenantAccessIndexReq
	TenantAccessIndexResp                 = sys.TenantAccessIndexResp
	TenantAccessMultiUpdateReq            = sys.TenantAccessMultiUpdateReq
	TenantAgreement                       = sys.TenantAgreement
	TenantAgreementIndexReq               = sys.TenantAgreementIndexReq
	TenantAgreementIndexResp              = sys.TenantAgreementIndexResp
	TenantAppIndexReq                     = sys.TenantAppIndexReq
	TenantAppIndexResp                    = sys.TenantAppIndexResp
	TenantAppInfo                         = sys.TenantAppInfo
	TenantAppMenu                         = sys.TenantAppMenu
	TenantAppMenuIndexReq                 = sys.TenantAppMenuIndexReq
	TenantAppMenuIndexResp                = sys.TenantAppMenuIndexResp
	TenantAppModule                       = sys.TenantAppModule
	TenantAppMultiUpdateReq               = sys.TenantAppMultiUpdateReq
	TenantAppWithIDOrCode                 = sys.TenantAppWithIDOrCode
	TenantConfig                          = sys.TenantConfig
	TenantConfigRegisterAutoCreateArea    = sys.TenantConfigRegisterAutoCreateArea
	TenantConfigRegisterAutoCreateProject = sys.TenantConfigRegisterAutoCreateProject
	TenantInfo                            = sys.TenantInfo
	TenantInfoCreateReq                   = sys.TenantInfoCreateReq
	TenantInfoIndexReq                    = sys.TenantInfoIndexReq
	TenantInfoIndexResp                   = sys.TenantInfoIndexResp
	TenantModuleCreateReq                 = sys.TenantModuleCreateReq
	TenantModuleIndexReq                  = sys.TenantModuleIndexReq
	TenantModuleIndexResp                 = sys.TenantModuleIndexResp
	TenantModuleWithIDOrCode              = sys.TenantModuleWithIDOrCode
	TenantOpenCheckTokenReq               = sys.TenantOpenCheckTokenReq
	TenantOpenCheckTokenResp              = sys.TenantOpenCheckTokenResp
	TenantOpenWebHook                     = sys.TenantOpenWebHook
	ThirdApp                              = sys.ThirdApp
	ThirdAppConfig                        = sys.ThirdAppConfig
	ThirdEmail                            = sys.ThirdEmail
	ThirdSms                              = sys.ThirdSms
	UserAreaApplyCreateReq                = sys.UserAreaApplyCreateReq
	UserAreaApplyDealReq                  = sys.UserAreaApplyDealReq
	UserAreaApplyIndexReq                 = sys.UserAreaApplyIndexReq
	UserAreaApplyIndexResp                = sys.UserAreaApplyIndexResp
	UserAreaApplyInfo                     = sys.UserAreaApplyInfo
	UserCaptchaReq                        = sys.UserCaptchaReq
	UserCaptchaResp                       = sys.UserCaptchaResp
	UserChangePwdReq                      = sys.UserChangePwdReq
	UserCheckTokenReq                     = sys.UserCheckTokenReq
	UserCheckTokenResp                    = sys.UserCheckTokenResp
	UserCodeToUserIDReq                   = sys.UserCodeToUserIDReq
	UserCodeToUserIDResp                  = sys.UserCodeToUserIDResp
	UserCreateResp                        = sys.UserCreateResp
	UserForgetPwdReq                      = sys.UserForgetPwdReq
	UserInfo                              = sys.UserInfo
	UserInfoCreateReq                     = sys.UserInfoCreateReq
	UserInfoDeleteReq                     = sys.UserInfoDeleteReq
	UserInfoIndexReq                      = sys.UserInfoIndexReq
	UserInfoIndexResp                     = sys.UserInfoIndexResp
	UserInfoReadReq                       = sys.UserInfoReadReq
	UserInfoUpdateReq                     = sys.UserInfoUpdateReq
	UserLoginReq                          = sys.UserLoginReq
	UserLoginResp                         = sys.UserLoginResp
	UserMessage                           = sys.UserMessage
	UserMessageIndexReq                   = sys.UserMessageIndexReq
	UserMessageIndexResp                  = sys.UserMessageIndexResp
	UserMessageStatistics                 = sys.UserMessageStatistics
	UserMessageStatisticsResp             = sys.UserMessageStatisticsResp
	UserProfile                           = sys.UserProfile
	UserProfileIndexReq                   = sys.UserProfileIndexReq
	UserProfileIndexResp                  = sys.UserProfileIndexResp
	UserRegisterReq                       = sys.UserRegisterReq
	UserRegisterResp                      = sys.UserRegisterResp
	UserRoleIndexReq                      = sys.UserRoleIndexReq
	UserRoleIndexResp                     = sys.UserRoleIndexResp
	UserRoleMultiUpdateReq                = sys.UserRoleMultiUpdateReq
	WeatherAir                            = sys.WeatherAir
	WeatherReadReq                        = sys.WeatherReadReq
	WeatherReadResp                       = sys.WeatherReadResp
	WithAppCodeID                         = sys.WithAppCodeID
	WithCode                              = sys.WithCode
	WithID                                = sys.WithID
	WithIDCode                            = sys.WithIDCode

	ModuleManage interface {
		ModuleInfoCreate(ctx context.Context, in *ModuleInfo, opts ...grpc.CallOption) (*WithID, error)
		ModuleInfoIndex(ctx context.Context, in *ModuleInfoIndexReq, opts ...grpc.CallOption) (*ModuleInfoIndexResp, error)
		ModuleInfoUpdate(ctx context.Context, in *ModuleInfo, opts ...grpc.CallOption) (*Empty, error)
		ModuleInfoDelete(ctx context.Context, in *WithIDCode, opts ...grpc.CallOption) (*Empty, error)
		ModuleInfoRead(ctx context.Context, in *WithIDCode, opts ...grpc.CallOption) (*ModuleInfo, error)
		ModuleMenuCreate(ctx context.Context, in *MenuInfo, opts ...grpc.CallOption) (*WithID, error)
		ModuleMenuIndex(ctx context.Context, in *MenuInfoIndexReq, opts ...grpc.CallOption) (*MenuInfoIndexResp, error)
		ModuleMenuUpdate(ctx context.Context, in *MenuInfo, opts ...grpc.CallOption) (*Empty, error)
		ModuleMenuDelete(ctx context.Context, in *WithID, opts ...grpc.CallOption) (*Empty, error)
	}

	defaultModuleManage struct {
		cli zrpc.Client
	}

	directModuleManage struct {
		svcCtx *svc.ServiceContext
		svr    sys.ModuleManageServer
	}
)

func NewModuleManage(cli zrpc.Client) ModuleManage {
	return &defaultModuleManage{
		cli: cli,
	}
}

func NewDirectModuleManage(svcCtx *svc.ServiceContext, svr sys.ModuleManageServer) ModuleManage {
	return &directModuleManage{
		svr:    svr,
		svcCtx: svcCtx,
	}
}

func (m *defaultModuleManage) ModuleInfoCreate(ctx context.Context, in *ModuleInfo, opts ...grpc.CallOption) (*WithID, error) {
	client := sys.NewModuleManageClient(m.cli.Conn())
	return client.ModuleInfoCreate(ctx, in, opts...)
}

func (d *directModuleManage) ModuleInfoCreate(ctx context.Context, in *ModuleInfo, opts ...grpc.CallOption) (*WithID, error) {
	return d.svr.ModuleInfoCreate(ctx, in)
}

func (m *defaultModuleManage) ModuleInfoIndex(ctx context.Context, in *ModuleInfoIndexReq, opts ...grpc.CallOption) (*ModuleInfoIndexResp, error) {
	client := sys.NewModuleManageClient(m.cli.Conn())
	return client.ModuleInfoIndex(ctx, in, opts...)
}

func (d *directModuleManage) ModuleInfoIndex(ctx context.Context, in *ModuleInfoIndexReq, opts ...grpc.CallOption) (*ModuleInfoIndexResp, error) {
	return d.svr.ModuleInfoIndex(ctx, in)
}

func (m *defaultModuleManage) ModuleInfoUpdate(ctx context.Context, in *ModuleInfo, opts ...grpc.CallOption) (*Empty, error) {
	client := sys.NewModuleManageClient(m.cli.Conn())
	return client.ModuleInfoUpdate(ctx, in, opts...)
}

func (d *directModuleManage) ModuleInfoUpdate(ctx context.Context, in *ModuleInfo, opts ...grpc.CallOption) (*Empty, error) {
	return d.svr.ModuleInfoUpdate(ctx, in)
}

func (m *defaultModuleManage) ModuleInfoDelete(ctx context.Context, in *WithIDCode, opts ...grpc.CallOption) (*Empty, error) {
	client := sys.NewModuleManageClient(m.cli.Conn())
	return client.ModuleInfoDelete(ctx, in, opts...)
}

func (d *directModuleManage) ModuleInfoDelete(ctx context.Context, in *WithIDCode, opts ...grpc.CallOption) (*Empty, error) {
	return d.svr.ModuleInfoDelete(ctx, in)
}

func (m *defaultModuleManage) ModuleInfoRead(ctx context.Context, in *WithIDCode, opts ...grpc.CallOption) (*ModuleInfo, error) {
	client := sys.NewModuleManageClient(m.cli.Conn())
	return client.ModuleInfoRead(ctx, in, opts...)
}

func (d *directModuleManage) ModuleInfoRead(ctx context.Context, in *WithIDCode, opts ...grpc.CallOption) (*ModuleInfo, error) {
	return d.svr.ModuleInfoRead(ctx, in)
}

func (m *defaultModuleManage) ModuleMenuCreate(ctx context.Context, in *MenuInfo, opts ...grpc.CallOption) (*WithID, error) {
	client := sys.NewModuleManageClient(m.cli.Conn())
	return client.ModuleMenuCreate(ctx, in, opts...)
}

func (d *directModuleManage) ModuleMenuCreate(ctx context.Context, in *MenuInfo, opts ...grpc.CallOption) (*WithID, error) {
	return d.svr.ModuleMenuCreate(ctx, in)
}

func (m *defaultModuleManage) ModuleMenuIndex(ctx context.Context, in *MenuInfoIndexReq, opts ...grpc.CallOption) (*MenuInfoIndexResp, error) {
	client := sys.NewModuleManageClient(m.cli.Conn())
	return client.ModuleMenuIndex(ctx, in, opts...)
}

func (d *directModuleManage) ModuleMenuIndex(ctx context.Context, in *MenuInfoIndexReq, opts ...grpc.CallOption) (*MenuInfoIndexResp, error) {
	return d.svr.ModuleMenuIndex(ctx, in)
}

func (m *defaultModuleManage) ModuleMenuUpdate(ctx context.Context, in *MenuInfo, opts ...grpc.CallOption) (*Empty, error) {
	client := sys.NewModuleManageClient(m.cli.Conn())
	return client.ModuleMenuUpdate(ctx, in, opts...)
}

func (d *directModuleManage) ModuleMenuUpdate(ctx context.Context, in *MenuInfo, opts ...grpc.CallOption) (*Empty, error) {
	return d.svr.ModuleMenuUpdate(ctx, in)
}

func (m *defaultModuleManage) ModuleMenuDelete(ctx context.Context, in *WithID, opts ...grpc.CallOption) (*Empty, error) {
	client := sys.NewModuleManageClient(m.cli.Conn())
	return client.ModuleMenuDelete(ctx, in, opts...)
}

func (d *directModuleManage) ModuleMenuDelete(ctx context.Context, in *WithID, opts ...grpc.CallOption) (*Empty, error) {
	return d.svr.ModuleMenuDelete(ctx, in)
}
