package areamanagelogic

import (
	"context"
	"gitee.com/i-Things/core/service/syssvr/internal/logic"
	"gitee.com/i-Things/core/service/syssvr/internal/repo/relationDB"
	"gitee.com/i-Things/core/service/syssvr/internal/svc"
	"gitee.com/i-Things/core/service/syssvr/pb/sys"
	"gitee.com/i-Things/share/def"
	"gitee.com/i-Things/share/oss/common"
	"gitee.com/i-Things/share/utils"
	"github.com/zeromicro/go-zero/core/logx"
)

func transPoArrToPbTree(ctx context.Context, svcCtx *svc.ServiceContext, root *relationDB.SysAreaInfo, poArr []*relationDB.SysAreaInfo) *sys.AreaInfo {
	pbList := make([]*sys.AreaInfo, 0, len(poArr))
	for _, po := range poArr {
		pbList = append(pbList, transPoToPb(ctx, po, svcCtx))
	}
	return buildPbTree(transPoToPb(ctx, root, svcCtx), pbList)
}

func buildPbTree(rootArea *sys.AreaInfo, pbList []*sys.AreaInfo) *sys.AreaInfo {
	// 将所有节点按照 parentID 分组
	nodeMap := make(map[int64][]*sys.AreaInfo)
	for _, pbOne := range pbList {
		nodeMap[pbOne.ParentAreaID] = append(nodeMap[pbOne.ParentAreaID], pbOne)
	}

	// 递归生成子树
	buildPbSubtree(rootArea, nodeMap)

	return rootArea
}

func transPoToPb(ctx context.Context, po *relationDB.SysAreaInfo, svcCtx *svc.ServiceContext) *sys.AreaInfo {
	parentAreaID := po.ParentAreaID
	if parentAreaID == 0 {
		parentAreaID = def.RootNode
	}
	if po.AreaImg != "" {
		var err error
		po.AreaImg, err = svcCtx.OssClient.PrivateBucket().SignedGetUrl(ctx, po.AreaImg, 24*60*60, common.OptionKv{})
		if err != nil {
			logx.WithContext(ctx).Errorf("%s.SignedGetUrl err:%v", utils.FuncName(), err)
		}
	}
	return &sys.AreaInfo{
		CreatedTime:     po.CreatedTime.Unix(),
		AreaID:          int64(po.AreaID),
		ParentAreaID:    parentAreaID,
		ProjectID:       int64(po.ProjectID),
		AreaName:        po.AreaName,
		AreaNamePath:    po.AreaNamePath,
		AreaIDPath:      po.AreaIDPath,
		Position:        logic.ToSysPoint(po.Position),
		Desc:            utils.ToRpcNullString(po.Desc),
		IsLeaf:          po.IsLeaf,
		IsSysCreated:    po.IsSysCreated,
		LowerLevelCount: po.LowerLevelCount,
		ChildrenAreaIDs: po.ChildrenAreaIDs,
		DeviceCount:     utils.ToRpcNullInt64(po.DeviceCount),
		UseBy:           po.UseBy,
		AreaImg:         po.AreaImg,
	}
}
func AreaInfosToPb(ctx context.Context, svcCtx *svc.ServiceContext, pos []*relationDB.SysAreaInfo) (ret []*sys.AreaInfo) {
	if pos == nil {
		return nil
	}
	for _, po := range pos {
		ret = append(ret, transPoToPb(ctx, po, svcCtx))
	}
	return
}

func buildPbSubtree(node *sys.AreaInfo, nodeMap map[int64][]*sys.AreaInfo) {
	// 找到当前节点的子节点数组
	children := nodeMap[node.AreaID]

	// 递归生成子树
	for _, child := range children {
		buildPbSubtree(child, nodeMap)
	}

	// 将生成的子树数组作为当前节点的子节点数组
	node.Children = children
}
