info(
	title: "租户管理模块"
	desc: "租户管理"
	author: "L"
	email: "174805676@qq.com"
	version: "v0.1.0"
)

@server (
	group: system/tenant/info
	prefix: /api/v1/system/tenant/info
	middleware:  CheckTokenWare,CheckApiWare, DataAuthWare,InitCtxsWare
)

service api {
    @doc "添加租户"
    @handler create
    post /create (TenantInfoCreateReq) returns (WithID)

    @doc "获取租户列表"
    @handler index
    post /index (TenantInfoIndexReq) returns (TenantInfoIndexResp)

    @doc "获取租户详情"
    @handler read
    post /read (WithIDOrCode) returns (TenantInfo)

    @doc "更新租户"
    @handler update
    post /update (TenantInfo) returns ()

    @doc "删除租户"
    @handler delete
    post /delete (WithIDOrCode) returns ()
}

@server (
    group: system/tenant/config
    prefix: /api/v1/system/tenant/config
    middleware:  CheckTokenWare,CheckApiWare, DataAuthWare,InitCtxsWare
)

service api {
    @doc "更新租户配置"
    @handler update
    post /update (TenantConfig) returns ()
    @doc "获取租户配置"
    @handler read
    post /read (WithCode) returns (TenantConfig)
}

@server (
    group: system/tenant/core
    prefix: /api/v1/system/tenant/core
    middleware:  InitCtxsWare
)
service api {
    @doc "获取租户信息"
    @handler read
    post /read (WithIDOrCode) returns (TenantCore)
}


@server (
    group: system/tenant/access/info
    prefix: /api/v1/system/tenant/access/info
    middleware:  CheckTokenWare,CheckApiWare, DataAuthWare,InitCtxsWare
)

service api {
    @doc "获取租户操作权限树"
    @handler tree
    post /tree (WithCode) returns (TenantAccessInfoTreeResp)
    @doc "获取租户操作权限树"
    @handler index
    post /index (WithCode) returns (TenantAccessInfo)
    @doc "批量更新租户操作权限"
    @handler multiUpdate
    post /multi-update (TenantAccessInfo) returns ()
}


@server (
    group: system/tenant/agreement
    prefix: /api/v1/system/tenant/agreement
    middleware:  CheckTokenWare,CheckApiWare, DataAuthWare,InitCtxsWare
)

service api {
    @doc "添加协议"
    @handler create
    post /create (TenantAgreement) returns (WithID)

    @doc "获取协议列表"
    @handler index
    post /index (TenantAgreementIndexReq) returns (TenantAgreementIndexResp)

    @doc "更新协议"
    @handler update
    post /update (TenantAgreement) returns ()

    @doc "删除协议"
    @handler delete
    post /delete (WithID) returns ()
}

@server (
    group: system/tenant/agreement
    prefix: /api/v1/system/tenant/agreement
    middleware:  InitCtxsWare
)

service api {
    @doc "获取协议详情"
    @handler read
    post /read (WithIDOrCode) returns (TenantAgreement)
}

type (
    TenantAgreement{
        ID int64 `json:"id,optional"`                                      // 编号
        Code string `json:"code,optional"`                               // 编号
        Name string `json:"name,optional"`                                 // 协议名称
        Title string `json:"title,optional"`//协议标题
        Content string `json:"content,optional"`//协议内容(只有详情会返回)
    }
    TenantAgreementIndexReq{
        Page *PageInfo `json:"page,optional"`                   // 分页信息
    }
    TenantAgreementIndexResp{
        List []*TenantAgreement `json:"list"`            // 列表数据
        Total int64 `json:"total"`                    // 列表总记录数
    }
)

type(
    TenantAccessInfo{
        Code string `json:"code"`                     // 租户编号
        AccessCodes []string `json:"accessCodes"`                               // 模块编号
    }
    TenantAccessInfoTreeResp{
        List []*AccessModuleInfo `json:"list"`
        Total int64 `json:"total"`
    }
)

type (

    TenantInfoCreateReq{
        Info *TenantInfo `json:"info"`
        AdminUserInfo *UserInfo `json:"adminUserInfo"`
    }

    TenantInfoCreateResp{
        Code string `json:"code"`
    }

    TenantInfoIndexReq  {
        Page *PageInfo `json:"page,optional"`                   // 分页信息,只获取一个则不填
        Name string `json:"name,optional"`                     // 应用名称
        Code string `json:"code,optional"`                     // 应用编号
        WithAdminUser bool `json:"withAdminUser,optional"` //同时获取管理员核心信息
    }

    TenantInfoIndexResp  {
        List []*TenantInfo `json:"list"`            // 租户列表数据
        Total int64 `json:"total"`                    // 租户列表总记录数
    }
    TenantConfig{
        TenantCode                string                                   `json:"tenantCode,optional"`  // 租户编码
        RegisterRoleID            int64                                    `json:"registerRoleID,optional"`  //注册分配的角色id
        WeatherKey                string `json:"weatherKey,optional"`//和风天气秘钥 参考: https://dev.qweather.com/
        CheckUserDelete           int64                                    `json:"checkUserDelete,optional"`  //是否检查用户注销 1(禁止项目管理员注销账号) 2(不禁止项目管理员注销账号)
        RegisterAutoCreateProject []*TenantConfigRegisterAutoCreateProject `json:"registerAutoCreateProject,optional"` //注册自动创建项目和区域
    }
    TenantConfigRegisterAutoCreateProject  {
        ProjectName  string                                   `json:"projectName"`
        IsSysCreated int64                                    `json:"isSysCreated"` //是否是系统创建的,系统创建的只有管理员可以删除
        Areas        []*TenantConfigRegisterAutoCreateArea `json:"areas,optional"`
    }
    TenantConfigRegisterAutoCreateArea  {
        AreaName     string `json:"areaName"`
        AreaImg      string `json:"areaImg,optional"`
        IsUpdateAreaImg bool   `json:"isUpdateAreaImg,omitempty,optional"`
        IsSysCreated int64  `json:"isSysCreated"` //是否是系统创建的,系统创建的只有管理员可以删除
    }
)
